package test.inacap.prueba2.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import test.inacap.prueba2.R;


public class DatosSesionFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public DatosSesionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_datos_sesion, container, false);

        final EditText etUsuario = (EditText) layout.findViewById(R.id.etUsername);
        final EditText etPass = (EditText) layout.findViewById(R.id.etPass);
        final Button btnRegistro  = (Button) layout.findViewById(R.id.btnRegistro);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = etUsuario.getText().toString().trim();
                String contraseñauser = etPass.getText().toString().trim();

                if (usuario.isEmpty()){
                    etUsuario.setError("Campo obligatorio");
                    etUsuario.requestFocus();
                }else if (contraseñauser.isEmpty()){
                    etPass.setError("Campo obligatorio");
                    etPass.requestFocus();
                }else {
                    Toast.makeText(getContext(), "Contraseña ingresada: " + contraseñauser, Toast.LENGTH_SHORT).show();
                }
            }
        });
        return layout;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("","");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String nombreFragmento, String evento);
    }
}
